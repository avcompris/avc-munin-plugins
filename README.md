# avc-munin-plugins

Some Munin plugins used on Avantage Compris’ technical infrastructure.

## docker_stat

See original plugin: [https://github.com/atommaki/munin-plugin-docker](https://github.com/atommaki/munin-plugin-docker)

Added Python 3.5 support for Debian 9.11.


